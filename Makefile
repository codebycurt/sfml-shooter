.PHONY: clean All

All:
	@echo "----------Building project:[ sfml-shooter - Debug ]----------"
	@"$(MAKE)" -f  "sfml-shooter.mk"
clean:
	@echo "----------Cleaning project:[ sfml-shooter - Debug ]----------"
	@"$(MAKE)" -f  "sfml-shooter.mk" clean
