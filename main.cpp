#include <cstdlib>
#include <ctime>
#include <SFML/Graphics.hpp>
#include <string>

using namespace std;

// global speed adjustment

float speedup = 40;  // multiplyer to increase speed of game 
sf::Font font;
string font_location = "/home/curt/coding/sfml-shooter/resources/arial.ttf";
int game_difficulty;

class player {

public:	

	int confirmed_kills;

	float x, y;
	sf::CircleShape PlayerIcon;
	sf::Color health_indicator_color;
	
	sf::CircleShape health_indicator;
	
	sf::Text health_display;
	
	sf::CircleShape face_indicator;
	
	const float step = 0.05 * speedup; // distance the player moves per keystroke
	const float size = 10; // player's (circle) size
	float health;
	int facing; // 1 = up, 2 = right, 3 = down, 4 = left
	bool can_rotate;
	
	void spawn() {
		
		confirmed_kills = 0;
		facing = 2;
		can_rotate = true;
		health = 100;
		
		health_indicator.setRadius(30);
		health_indicator_color = sf::Color::Green;
		health_indicator.setPosition(27, 25);
		health_indicator.setFillColor(health_indicator_color);
		
		face_indicator.setRadius(3);
		face_indicator.setFillColor(sf::Color::White);
		
		health_display.setCharacterSize(20);
		health_display.setPosition(40, 40);
		health_display.setFont(font);
		
		int r_xpos = rand() % 400 + 200;
		x = float(r_xpos);	
		
		int r_ypos = rand() % 300 + 200;
		y = float(r_ypos);
		
		PlayerIcon.setRadius(size);
		PlayerIcon.setFillColor(health_indicator_color);		
		
	}
	
	void rotate() {
		if (!can_rotate) {
			// do nothing
		} else {	
			facing++;
				if (facing == 5) {
					facing = 1;
				}
		}
	}
	
	void draw(sf::RenderWindow &window) {
		
		PlayerIcon.setPosition(x, y);
		
		switch (facing) {
			
			/* up */ case 1: face_indicator.setPosition(x+7, y-7); break;
			/* rt */ case 2: face_indicator.setPosition(x+21, y+7); break;
			
			/* dn */ case 3: face_indicator.setPosition(x+7, y+21); break;
			/* lt */ case 4: face_indicator.setPosition(x-7, y+7); break;

		}

		health_display.setString(to_string(int(health)));  // damage is done as float but shown as int

		window.draw(PlayerIcon);
		window.draw(face_indicator);
		window.draw(health_indicator);		
		window.draw(health_display);
	}
	
	void damage() {
		
		float dpc = 0.1; // damage per cycle
		health_indicator_color = sf::Color::Red;
		PlayerIcon.setFillColor(sf::Color::Red);
		health -= dpc;
	}
	
	void healthy() {
		health_indicator_color = sf::Color::Green;
		PlayerIcon.setFillColor(sf::Color::Green);
	}
	
	void move_left() {
		if (x > 25) {
			x -= step;
		}
	}
	
	void move_right() {
		if (x < 750) {
			x += step;
		}
	}
	
	void move_up() {
		if (y > 25) {
			y -= step;
		}
	}
	
	void move_down() {
		if (y < 550) {
			y += step;
		}
	}

};

class enemy {
	
public:

	float x, y;		
	float step = 0.07 * speedup;
	float size;
	float health;
	sf::CircleShape EnemyIcon;
	
	void spawn() {

		int r_xpos, r_ypos;
		// spawn enemies toward extremities of screen
		
		int location = rand() % 4 + 1;
		
		switch(location) {
		
			// Spawn at top of screen
			case 1: r_xpos = rand() % 800 + 1;
					r_ypos = rand() % 50 + 1;
					break;
			
			// Spawn at left of screen
			case 2: r_xpos = rand() % 50 + 1;
					r_ypos = rand() % 600 + 1;
					break;
			
			// Spawn at bottom of screen
			case 3: r_xpos = rand() % 800 + 1;
					r_ypos = rand() % 50 + 550;
					break;
			
			// Spawn at right of screen
			case 4: r_xpos = rand() % 50 + 750;
					r_ypos = rand() % 600 + 1;
					break;
		}
		
		x = float(r_xpos);	
		y = float(r_ypos);
		
		// We'll have dynamic size based on health in the future
		
		health = 100; // rand() % 100 + 10;
		size = 5; // (health + 10) / 2;
		
		EnemyIcon.setRadius(size);
		EnemyIcon.setFillColor(sf::Color::Red);
		
	}
	
	void damage() {
	
		float dpc = 0.1; // damage per cycle
		health -= dpc;
	}
			
	void reset() {
		step = 0.05 * speedup;
		spawn();
	}
	
	void move(float px, float py) {
		px = px + 5; // offset for the center of the player's circle icon
		py = py + 5;
		int freewill = rand() % 100 + 1;		

		if (freewill > 60) {
		EnemyIcon.setFillColor(sf::Color::Color::Red);
		int direction = rand() % 4 + 1;
		//int multiplier = rand() % 12 + 5;
		int multiplier = 1;
			switch (direction) {
		
				case 1: x += step * multiplier; break;
				case 2: x -= step * multiplier; break;
				case 3: y += step * multiplier; break;
				case 4: y -= step * multiplier; break;
			}
			
		} else {
			EnemyIcon.setFillColor(sf::Color::Yellow);
			int seek = rand() % 2 + 1;  /* randomly seek x or y, try to keep enemies from clustering too much */
			
				switch(seek) {
					
				case 1: if (x < px) {
							x += step;
						}
						if (x > px) {
							x -= step;
						}
						break;
					
				case 2: if (y < py) {
							y += step;
						}
						if (y > py) {
							y -= step;
						}
						break;
				}
				
				
		}
	}
	
	void draw(sf::RenderWindow &window) {
		EnemyIcon.setPosition(x, y);
		window.draw(EnemyIcon);
	}
	
};

class bullet {
public:
	
	float x, y;
	float step = 0.1 * speedup;
	float size = 3;
	int direction;
	
	sf::CircleShape bulletIcon;
	
	void fire(float startx, float starty) {
		float x_offset = 7;
		float y_offset = 7;
		
		x = startx + x_offset;
		y = starty + y_offset;
		bulletIcon.setRadius(size);
		bulletIcon.setFillColor(sf::Color::White);
		bulletIcon.setPosition(x, y);
	}
	
	void move() {
		// up
		if (direction == 1) {
			y-=step; // for now this will fire only up. Eventually fire toward a mouse pointer
		}
		
		// right
		if (direction == 2)
			x+=step;
		
		// down
		if (direction == 3) {
			y+=step;
		}
		
		// left
		if (direction == 4) {
			x-=step;
		}
	}
	
	void draw(sf::RenderWindow &window) {
		bulletIcon.setPosition(x, y);
		window.draw(bulletIcon);
	}

};

class enemy_manager {

	public:
	vector<enemy*> moblist;
	vector<bullet*> bulletlist;
	
	bool loaded = false;
	
	int max_enemies = 100;
	// int max_bullets = 10;
	

	void shoot(float px, float py, int to_direction) {
		if (!loaded) {
			// do nothing
		} else {
		bullet* pBullet = new bullet;
		
		switch (to_direction) {
			case 1: py-=20; break;
			case 2: px+=20; break;
			case 3: py+=20; break;
			case 4: px-=20; break;
		}
		
		pBullet->direction = to_direction;
		pBullet->fire(px, py);
		bulletlist.push_back(pBullet);
		
		}
	}

	void spawnMobs(int number) {
		
		for (int i=0; i<number; i++) {
			if (moblist.size() <= max_enemies) {
				enemy* pEnemy = new enemy;
				pEnemy->spawn();
				moblist.push_back(pEnemy);
			}
		}
    }
	
	void update(float px, float py, sf::RenderWindow &window) {
		// Update and Draw Enemies
		for (unsigned int i=0; i < moblist.size(); i++) {
			moblist[i]->move(px, py);
			moblist[i]->draw(window);
		}
		
		// Update and Draw Bullets
		for (unsigned int i=0; i < bulletlist.size(); i++) {
			bulletlist[i]->move();
			bulletlist[i]->draw(window);
			
			// Get rid of bullets that go off screen
			if (bulletlist[i]->x < 0 || bulletlist[i]->x > 800 || 
				bulletlist[i]->y < 0 || bulletlist[i]->y > 600) {
					
				bulletlist.erase(bulletlist.begin() + i);
				
			}
		}
		
	}
	
	void collision_detect(player &p1) {
		int offset = -5;
		bool collision = false;
		
		for (unsigned int i = 0; i < moblist.size(); i++) {
			// Detect Enemy Collision with Player and Damage Both Accordingly
			if ((abs(moblist[i]->x - p1.x + offset) < 12 &&
			     abs(moblist[i]->y - p1.y + offset) < 12)) {
				 p1.damage();
				 moblist[i]->damage();
				 collision = true;
			}
		
			// Detect Enemy Collsion with Bullets and Damage Enemies Accordingly
			for (unsigned int q = 0; q < bulletlist.size(); q++) {
				if ((abs(moblist[i]->x - bulletlist[q]->x) < 7 &&
			         abs(moblist[i]->y - bulletlist[q]->y) < 7)) {
					
					// each bullet hurts the mob
					// currently damage is set at min=34 - [avg ~ 95] - max=129
					moblist[i]->health-=(rand() % 85 + 34);
					bulletlist.erase(bulletlist.begin() + q);
				}
			}
			
			if (moblist[i]->health <= 0) {
				moblist.erase(moblist.begin() + i);
				p1.confirmed_kills++;
			}	
			
		} // end moblist collision for loop
		
		// turn the health indicator back to green if no collision
		if (!collision) {
			p1.healthy();
		}
	}
	
	void speed_up(float inc) {
		for (unsigned int i = 0; i < moblist.size(); i++) {
			moblist[i]->step += inc;
		}
	}
	
};

class menu {

private:

	sf::Text menu_dialog;
	
public:
	
	void initMenu() {
		menu_dialog.setCharacterSize(20);
		menu_dialog.setPosition(250.f, 200.f);
		menu_dialog.setFont(font);
	}
	
	void pause(sf::RenderWindow &window) {

		initMenu();
		
		while (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
		// do nothing; wait for the previous ESC input to end.
		}
	
		menu_dialog.setString("It's a pause menu for Some Kind of Game!\n\nEsc Exits.\n\nEnter Continues.");
	
		while (true) {
			
			window.clear();
			window.draw(menu_dialog);
			window.display();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
				window.close();
				exit(0);
			}
			
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
				break;
			}
			
		}
	}
	
	void game_start(sf::RenderWindow &window) {
		
		initMenu();
		
		while (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
		// do nothing, wait for the previous ESC input to end.
		}
	
		menu_dialog.setString("It's Some Kind of Game!\n\n    Movement: WASD or Arrow Keys\n\n    Firing: Mouse Left-Click\n\n    Rotate: Mouse Right-Click\n\n    Esc Exits.\n\nPress Enter to Play.");
		
		while (true) {
			
			window.clear();
			window.draw(menu_dialog);
			window.display();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
				window.close();
				exit(0);
			}
			
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
				break;
			}
		}
	
	}


	void game_over(int time_alive, int player_kills, sf::RenderWindow &window) {
	
		initMenu();

		menu_dialog.setString("You lasted " + to_string(time_alive) + " seconds!\n\nYou killed " + to_string(player_kills) + " enemies!\n\nEsc Exits.\nEnter Restarts.");
	
		while (true) {
			
			window.clear();
			window.draw(menu_dialog);
			window.display();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
				window.close();
				exit(0);
			}
			
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
				break;
			}
		}
	}	
};

void controls(player &Player1, enemy_manager &EM, sf::Clock &reload_clock) {
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
            Player1.move_left();
        }
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
            Player1.move_right();
        }
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
            Player1.move_up();
        }
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
            Player1.move_down();
        }	
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
            Player1.move_left();
        }
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
            Player1.move_right();
        }
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
            Player1.move_up();
        }
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
            Player1.move_down();
        }
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
			EM.shoot(Player1.x, Player1.y, Player1.facing);
			EM.loaded = false;
			reload_clock.restart();
        }
		
		if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
			Player1.rotate();
			Player1.can_rotate = false;
			reload_clock.restart();
		}
		        
 
}

void show_time(int game_time, int player_kills, sf::RenderWindow &window) {
	
		sf::Text time_display;
		time_display.setCharacterSize(20);
		time_display.setPosition(720, 40);
		time_display.setFont(font);
		time_display.setString(to_string(game_time) + " | " + to_string(player_kills));
		window.draw(time_display);
}

int main()
{	
	srand(time(0));
	font.loadFromFile(font_location);
	
	sf::Clock game_clock;
	sf::Clock difficulty_clock;
	sf::Clock reload_clock;
	
	sf::RenderWindow window(sf::VideoMode(800, 600), "Game Dev!");
	window.setFramerateLimit(60);
	
	game_difficulty = 0;
	
	menu Menus;
	player Player1;
	enemy_manager EM;
	//bullet Bullet1;
	
	int elapsed_time = 0; 
	int game_time = 0;
	int difficulty_ctr = 0;
	int reload_time = 40; // in milliseconds
	int fire_rate_limiter;
	
	EM.spawnMobs(4);
	
	Menus.game_start(window);
	
	reload_clock.restart();
	game_clock.restart();
	difficulty_clock.restart();
	
	Player1.spawn();
	
	while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event)) {
            
			if (event.type == sf::Event::Closed) {
                window.close();
			}
			
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
				Menus.pause(window);
				game_clock.restart();
				difficulty_clock.restart();
			}
		}
		
        window.clear();
		
		fire_rate_limiter = reload_clock.getElapsedTime().asMilliseconds();
		if (fire_rate_limiter > reload_time) {
			EM.loaded = true;
			Player1.can_rotate = true;
		}
		
		// Manage Game Clock and Difficulty Clock
		elapsed_time = game_clock.getElapsedTime().asMilliseconds();
		
		if (elapsed_time > 1000) {
			game_time += 1;
			game_clock.restart();

		}
		
		difficulty_ctr = difficulty_clock.getElapsedTime().asSeconds();
		if (difficulty_ctr > (rand() % (game_difficulty+1) + 4)) {

			int spawn_number = rand() % 5 + game_difficulty;
			EM.spawnMobs(spawn_number); // create a few more mobs
			EM.speed_up(0.01 + (game_difficulty/100) - (game_difficulty/200)); // speed up all mobs
			game_difficulty++;
			difficulty_clock.restart();
			
		}
		
		// display time counter in upper-right
		show_time(game_time, Player1.confirmed_kills, window);
		
		// player controls
		controls(Player1, EM, reload_clock);
				
		// collision detection
		EM.collision_detect(Player1);

		// draw enemies
		EM.update(Player1.x, Player1.y, window);

		// draw player
		Player1.draw(window);

		// player death
		if (Player1.health <= 0) {
			
			Menus.game_over(game_time, Player1.confirmed_kills, window);
			
			// restart game if player so decides
			Player1.spawn();
			
			// reset enemies
			EM.moblist.clear();
			EM.spawnMobs(4);

			// reset clocks
			game_clock.restart();
			difficulty_clock.restart();
			reload_clock.restart();
			game_time = 0;
			
			// reset difficulty
			game_difficulty = 0;
			difficulty_ctr = 0;
		}
		
        window.display();
	}

    return 0;
}